def program_requirements():
    print("Program Requirements:")
    print("1. Sets (Python data structure): mutable, heterogeneous, unorderd sequence of elements, *but* cannot hold duplicate values:)")
    print("2. Sets are mutable/changeable --  that is, can insert, update, delete.")
    print("3. while sets are mutable/ changeable they cannot containother mutable items like list, set, or dictionary")
    print("4. Note: an iterable is any object which can be iterated over --  that is lists, tuples, or strings.")
    print("5. Create a program that mirrors the following IPO (input/process/output) format.")
    print("6. Sets (Python data structure): mutable, heterogeneous, unorderd sequence of elements, *but* cannot hold duplicate values:)")
    print("\t a. Sets are mutable/changeable ")
    print("\t b. that is, can insert, update, delete.")
    
    

def using_dictionaries():

    fname = ""
    lname = ""
    degree = ""
    gpa = ""
    my_dictionary = {}
    print()
    print("Input:")
    fname = input("First Name: ")
    lname = input("Last Name: ")
    degree = input("Degree: ")
    major = input("Major: ")
    gpa = input("GPA: ")

    print()

    my_dictionary['fname'] = fname
    my_dictionary['lname'] = lname
    my_dictionary['degree'] = degree
    my_dictionary['major'] = major
    my_dictionary['gpa'] = gpa

    print("Output: ")

    print()

    print("Print my_dictionary:")
    print(my_dictionary)
    print()
    print("Return view of dictionary's pair, build in function:")
    print(my_dictionary.items())
    print()
    print("Return view object of all keys, built in function:")
    print(my_dictionary.keys())
    print()
    print("Return view object of all values in dictionary, built in function:")
    print(my_dictionary.values())
    print()
    print("Print only first and last names using keys:")
    print(my_dictionary['fname'], my_dictionary['lname'])
    print()
    print("Count number of items:")
    print(len(my_dictionary))
    print()
    print("Remove last dictionary item:")
    my_dictionary.popitem()
    print(my_dictionary)
    print()
    print("Delete major from dictionary, using key:")
    my_dictionary.pop('major')
    print(my_dictionary)
    print()
    print("Return object type:")
    print(type(my_dictionary))
    print()
    print("Delete all items from list:")
    my_dictionary.clear()
    print(my_dictionary)
