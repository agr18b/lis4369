def program_requirements():
    print("Python Lists")
    print()
    print("Program Requirements:")
    print("1. Lists (Python data strucutre): mutable, ordered sequence of elements.")
    print("2. Lists are mutable/changeable--that is, can insert, update, delete.")
    print('3. Create list - using square brackets [lists]: my_list = ["Cherries", "apples", "bananas", "oranges"]')
    print('4. Create a program that mirrors the following IPO (input/process/output) format.')
    print("Note: user enters number of requrested list elements, dynamically rendered below(that is, number o flements can change each run).")
    print()

def using_lists():
    
    start = 0
    end = 0
    my_list = []


    print("Input:")
    num = int(input("Enter number of list elements: "))

    print()

    for i in range(num):
        my_element = input('Please enter list element' + str(i + 1) + ": ")
        my_list.append(my_element) #append each element to end of list

    print("\nOutput:")
    print("Print my_list:")
    print(my_list)

    elem = input("\nPlease enter list element: ")
    pos = int(input("Please enter list index position (note: must convert to int): "))

    print("\nInsert element into specific position in my_list")
    my_list.insert(pos, elem)
    print(my_list)

    print("\nCount number of elements in list: ")
    print(len(my_list))

    print("\nSort elements in list alphabetically: ")
    my_list.sort()
    print(my_list)

    print("\nReverse list: ")
    my_list.reverse()
    print(my_list)

    print("\nRemove last list element: ")
    my_list.pop()
    print(my_list)

    print("\nDelete second element from list by index (note: 1=2nd element): ")
    del my_list[1]
    print(my_list)

    print("\nDelete element form list by *value* (cherries):")
    my_list.remove("cherries") 
    print(my_list)

    print("\nDelete all elements from list:")
    my_list.clear()
    print(my_list)  
