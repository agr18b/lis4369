def get_requirements():
    print("Program Requirements:")
    print("1. Program converts user-entered temperatures into Fahrenheit or Celsius scales.")
    print("2. Program continues to prompt for user entry until no longer requested.")
    print("3. Note: upper or lower case letters permiteed. Though, incorrect entires are not permitted.")
    print("4. Note: Porgram does not validate numeric data (Optional Requirement)")


def temp_conversion():

    temperature = 0.0
    choice = ' '
    type = ' '

    print()
    print("Input:")
    print()
    
    choice = input("Do you want to convert a temperature (y or n)?: ").lower()
    print()

    while (choice[0] == 'y'):
        type = input("Fahrenheit to Celsius? Type f or for celsius to fahrenheit type c: ").lower()

        if type[0] == 'f':
            temperature = float(input("Enter temperature in Fahrenheit: "))
            temperature = ((temperature - 32)*5)/9
            print("Tempreature in clesius = " +str(temperature))
            choice = input("\nDo you want to convert another temperature (y or n)?: ")
            print()
            
        elif type[0] == 'c':
            temperature = float(input("Enter temperature in Celsius: "))
            temperature = (temperature *9/5) + 32
            print("Tempreature in Fahrenheit = " +str(temperature))
            choice = input("\nDo you want to convert another temperature (y or n)?: ")
            print()
            
        else:
            print("incorrect entry please try again.")
            choice = input("Do you want to convert a temperature (y or n)?: ").lower()
            print()

    print("Thank you for using our temperature conversion program.")
