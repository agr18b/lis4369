def program_requirements():

    print("\nPython Calculator with Error Handling")

    print("\nProgram Requirements:")
    print("1. Program calculates two numbers, and rounds to two decimal places")
    print("2. Prompt user for tow numbers, and a suitable operator")
    print("3. Use Python error handling to validate data.")
    print("4. Test for correct arithmetic operator.")
    print("5. Division by zero not permitted")
    print("6. Note: Program loops until correct input entered - numbers and arithmetic operator")
    print()

def error_handling():

    while True:
        try:
            num1 = float(input("Enter num1: "))
            print()
            break
    
        except ValueError:
            print("Not valid number!")

    while True:
        try:
            num2 = float(input("Enter num2: "))
            print()
            break

        except ValueError:
            print("Not Valid number!")
    print()
    print("Suitable Operators: +, -, *, /, //, %, **")

    operator = input("Enter Operator: ")

    op_list = ['+', '-', '*', '/', '//', '%', '**']

    while True:
        if operator in op_list:
            break
        else:
            print("Incorrect operator")
            operator = input("Enter Operator: ")
            continue

    if operator in ('+', '-', '*', '**'):
        result = eval(str(num1) + operator + str(num2))
        print(result)

    elif operator == '/':
        while True:
            try:
                result = eval(str(num1) + operator + str(num2))
                print(result)
                break
            except ZeroDivisionError as err:
                print()
                print("Cannot divide by zero!")
                num2 = float(input("Enter num2: "))
                continue

    elif operator == '//':
        while True:
            try:
                result = eval(str(num1) + operator + str(num2))
                print(result)
                break
            except ZeroDivisionError as err:
                print()
                print("Cannot divide by zero!")
                num2 = float(input("Enter num2: "))
                continue

    elif operator == '%':
        while True:
            try:
                result = eval(str(num1) + operator + str(num2))
                print(result)
                break
            except ZeroDivisionError as err:
                print("Cannot divide by zero!")
                num2 = float(input("Enter num2: "))
                continue

    else:
        print("Incorrect operator!")

    print("Thank you for using my calculator!")

    #result = eval(str(num1) + operator + str(num2))
    #print(round(result,2))


