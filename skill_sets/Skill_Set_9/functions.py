def program_requirements():
    print("Python sets - like mathematical sets!")
    print()
    print("Program Requirements:")
    print("1. Sets (Python data structure): mutable, heterogeneous, unorderd sequence of elements, *but* cannot hold duplicate values:)")
    print("2. Sets are mutable/changeable --  that is, can insert, update, delete.")
    print("3. while sets are mutable/ changeable they cannot containother mutable items like list, set, or dictionary")
    print("\telements contained in set must be immutable")
    print("4. also since sets are unorderd, cannont use indexing to acces, update, or delete elements.")
    print("5. Two methods to create sets:")
    print('\t a. Create set using curly brackets: my_set = {1, 3.14, 2.0, "four", "Five"}')
    print("\t b. Create set using set() funciton: my_set = set(<iterable>)")
    print("6. Note: an iterable is any object which can be iterated over --  that is lists, tuples, or strings.")
    print("7. Create a program that mirrors the following IPO (input/process/output) format.")
    print()
    print("Input: hard coded, no user input")
    print("***NOTE*** All three sets below print as 'sets', regardless of how they were created.")


def sets():
    print()

    my_set = {1, 2.0, 3.14, 'four', 'Five'}

    print("Print my_set created using curly brackets:")
    print(my_set)
    print()

    print("Print type of my_set")
    print(type(my_set))
    print()


    my_set1 = set([1, 3,14, 2.0, 'four', 'Five'])

    print("Print my_set1 created using set() function with list: ")
    print(my_set1)
    print()

    print("Print type of my_set1")
    print(type(my_set1))
    print()

    my_set2 = set((1, 3.14, 2.0, 'four', 'Five'))
    print("Print my_set2 created using set funciton with tuple:")
    print(my_set2)
    print()

    print("Print type of my_set2:")
    print(type(my_set2))
    print()

    print("Length of my_set")
    print(len(my_set))
    print()

    print('Discard "four":')
    my_set.discard('four')
    print(my_set)
    print()

    print('Remove "Five":')
    my_set.discard('Five')
    print(my_set)
    print()

    print("Length of my_set")
    print(len(my_set))
    print()

    print("Add element to set (4) using add() method:")
    my_set.add(4)
    print(my_set)
    print()

    print("Display minimum number:")
    print(min(my_set))

    print("Display maximum number:")
    print(max(my_set))
    print()

    print("Display sum of numbers:")
    print(sum(my_set))
    print()

    print("Delete all set elements:")
    my_set.clear()
    print(my_set)
    print()

    print("Length of my_set:")
    print(len(my_set))
