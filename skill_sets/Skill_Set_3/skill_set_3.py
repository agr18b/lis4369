def description():
    print("IT/ICT Student Percentage")
    print()
    print("Program Requirements:")
    print("1. Find number of IT/ICT students in class.")
    print("2. Calculate IT/ICT Student Persentage.")
    print("3. Must use float data type (to facilitate right-alignment)")
    print("4. Format, right-align numbers, and round to two decimal places.")

def calculations():
    print()
    print("Input:")
    it = int(input("Enter number of IT students: "))
    ict = int(input("Enter number of ICT students: "))

    totalstudents = it + ict
    #print(totalstudents)
    itpercentage = float((it / totalstudents))
    ictpercentage = float((ict / totalstudents))
    #print(itpercentage)
    #print(ictpercentage)

    print("\nOutput:")
    print("{0:17} {1:>5.2f}".format("Total Students: ", totalstudents))
    print("{0:17} {1:>5.2%}".format("IT Students: ", itpercentage))
    print("{0:17} {1:>5.2%}".format("ICT Students: ", ictpercentage))
    print()

def main():
    description()
    calculations()

main()