def Program_Requirements():
    print("Calorie Percentage")
    print()
    print("Program Requirements:")
    print("1. Find calories per grams of fat, carbs, and protiens.")
    print("2. Calculate percentages.")
    print("3. Must Use float data types.")
    print("4. Format, right-align numbers, and round to two decimal places.")

def calculate_calorie_percentage():
    fatCal = 0.0
    carbCal = 0.0
    proteinCal = 0.0
    total = 0.0
    fatTot = 0.0
    carbTot = 0.0 
    proteinTot = 0.0


    print()
    print("Input:")
    fat = float(input("Enter total fat grams: "))
    carb = float(input("Enter total carb grams: "))
    protein = float(input("Enter total protein grams: "))

    fatCal = fat * 9
    carbCal = carb * 4
    proteinCal = protein * 4
    total = fatCal + carbCal + proteinCal

    fatTot = fatCal / total
    carbTot = carbCal / total
    proteinTot = proteinCal / total

    print()
    print("Output:")
    print("{0:17} {1:17} {2:17}".format("Type", "Calories", "Percentage"))
    print("{0:17} {1:,.2f} {2:>18.2%}".format("Fat", fatCal, fatTot))
    print("{0:17} {1:,.2f} {2:>18.2%}".format("Carb", carbCal, carbTot))
    print("{0:17} {1:,.2f} {2:>17.2%}".format("Protein", proteinCal, proteinTot))