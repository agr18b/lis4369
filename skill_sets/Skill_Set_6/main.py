import functions as f

def main():
    f.program_requirements()
    f.while_loop()
    f.for_loop_one_arg()
    f.for_loop_two_arg()
    f.for_loop_three_args()
    f.for_loops_three_args_negative_interval()
    f.for_loop_implicit()
    f.for_explicit_string_list()
    f.for_loop_break()
    f.for_loop_stop_continue()
    f.list_length()
    
    
if __name__ == "__main__":
    main()

