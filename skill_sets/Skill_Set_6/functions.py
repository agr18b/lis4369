def program_requirements():
    print("Program Requirements:")
    print("1. Print while loop.")
    print("2. Print for loops using range () funciton, and implicit and explicit lists.")
    print("3. Use break and continue statements.")
    print("4. Replicate display window.")
    print("Note: In Python, for loop used for iterating over a sequence (i.e. , list, tuple, dictionary, set, or string.)")

def while_loop():
    print()
    print("1. While loop:")
    x = 1

    while x < 4 :
        print(x)
        x += 1
#while_loop()

def for_loop_one_arg():
    print()
    print("2. for loop: using range() with 1 arg")

    for i in range (4):
        print(i)
#for_loop_one_arg()

def for_loop_two_arg():
    print()
    print("3. for loop: using range() funciton with two args:")

    for i in range (0, 4):
        print(i)
#for_loop_two_arg()

def for_loop_three_args():
    print()
    print("4. for loop: using range fucntion with three args (interval 2)")
    
    for i in range (1, 4, 2):
        print(i)
#for_loop_three_args()

def for_loops_three_args_negative_interval():
    print()
    print("5. for loop: using range() funciton with three args (negative interval):")

    for i in range (3, 0 ,-2):
        print(i)
#for_loops_three_args_negative_interval()

def for_loop_implicit():
    print()
    print("6. for loop using (implicit) list:")

    numberlist = [1,2,3]
    for i in numberlist:
        print(i)
#for_loop_implicit()

def for_explicit_string_list():
    print()
    print("7. for loop iterating through (explicit) string list:")

    stringlist = ["Michigan" ,"Alabama" ,"Florida"]

    for i in stringlist:
        print(i)
#for_explicit_string_list()

def for_loop_break():
    print()
    print("8. for loop using break statement (stops loop):")

    stringlist = ["Michigan" ,"Alabama" ,"Florida"]
    for i in stringlist:
        if i == "Florida":
            break
        else:
            print(i)
#for_loop_break()

def for_loop_stop_continue():
    print()
    print("9. for loop using continue statement (stops and continues with next):")
    
    skip = False
    stringlist = ["Michigan" ,"Alabama" ,"Florida"]
    
    for i in stringlist:
        if i == "Alabama":
            skip = True
            continue
        print(i)
#for_loop_stop_continue()

def list_length():
    print()
    print("10. print list legnth:")

    stringlist = ["Michigan" ,"Alabama" ,"Florida"]

    print(len(stringlist))
#list_length()
