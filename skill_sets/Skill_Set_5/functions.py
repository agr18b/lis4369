import sys

def get_requirements():
    print()
    print("Program Requirements:")
    print()
    print("1. Use Python selection structure.")
    print("2. Prompt user for two numbers, and a suitable operator")
    print("3. Test for correct numeric operator.")


def calculator_input():

    print()
    num1 = int(input("Enter num1: "))
    #if num1 != int:
    #    print("Please enter a number.")
    #    calculator_input()
    #else:
    #    num2 = input("Enter num2: ")
    num2 = int(input("Enter num2: "))
    print()

    print("Suitable Operators: +, -, *, /, // (integer division), % (Modulo operator), ** (power)")

    operator = input("Enter operator: ")
    print()
    #print(operator)

    if operator in ['+', '-', '*', '/', '//', '%', '**']:
        result = eval(str(num1) + operator + str(num2))
        print("Result: ", result)
    else:
        print("Incorrect Operator!")
        sys.exit
