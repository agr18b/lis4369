> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Assignment 4 Requirements:

*Three Parts:*

1. Backwards-engineer (using Python) the demo.py file given
2. The program should be organized with two modules:
	- functions.py module contains the following functions:
		a. get_requirements()
		b. data_analysis()
	- main.py module imports the functions.py module, and calls the functions within the main.py program
3. Provide screenshot of the graph created

#### README.md file should include the following items:

* Screenshot of graph created using the code.

#### Assignment Screenshots:

*Screenshot of Graph:

![graph created](img/img1.PNG)
