> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Assignment 3 Requirements:

*Two Parts:*

1. Backwards-engineer (using Python) the assignment given
2. The program should be organized with two modules:
	- functions.py module contains the following functions:
		a. get_requirements()
		b. estimate_painting_costs()
	- main.py module imports the functions.py module, and calls the functions within the main.py program

#### README.md file should include the following items:

* Screenshots of painting_cost application running twice within the same running program.

#### Assignment Screenshots:

*Screenshot of painting_cost application running:

![painting cost application](img/img1.PNG)
