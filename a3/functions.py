def get_requirements():
    print()
    print("Program Requirements:")
    print("1. Calculate home interior paint cost (w/p primer).")
    print("2. Must use float data types.")
    print("3. Must use SQFT_PER_GALLON constant (350)")
    print('4. Must use iteration structure (aka "loop")')
    print("5. Format, right-align numbers, and round to two decimal places.")
    print("6. Create at least five functions that are called by the program:")
    print("\ta. main(): calls two other functions get_requirements() and estimate_painting_cost()")
    print("\tb. get_requirements(): displays the program requiremts.")
    print("\tc. estimate_painting_cost(): calculates interior home painting, and calls print function")
    print("\td. print_painting_estimate(): displays paiting costs.")
    print("\te. print_painting_percentage(): displays painting costs percentages.")


def estimate_painting_cost():

#initalize variables
    total_interior_sqft = 0.0
    price_per_gallon_paint = 0.0
    hourly_painting_rate_per_sqft = 0.0

#get user data
    print("\nInput:")
    total_interior_sqft = (float(input("Enter total interior sq ft: ")))
    price_per_gallon_paint = (float(input("Enter price per gallon paint: ")))
    hourly_painting_rate_per_sqft = (float(input("Enter hourly painting rate per sq ft: ")))

 #calulations
    total_sqft = total_interior_sqft
    sqft_per_gallon = 350.00
    number_of_gallons = (total_interior_sqft / sqft_per_gallon)
    paint_per_gallon = price_per_gallon_paint
    labor_per_sqft = hourly_painting_rate_per_sqft

    paint_cost = (number_of_gallons * paint_per_gallon)
    labor_cost = (total_sqft * labor_per_sqft)
    total_cost = (paint_cost + labor_cost)

    paint_percent = (paint_cost / total_cost)
    labor_percent = (labor_cost / total_cost)
    total_percent = (paint_percent + labor_percent)

# display results
    print("\nOutput:")
    print("{0:17} {1:>17}".format("Item", "Amount"))
    print("{0:28} {1:,.2f}".format("Total sq ft", total_sqft))
    print("{0:28} {1:,.2f}".format("Sq Ft per Gallon:", sqft_per_gallon))
    print("{0:28} {1:,.2f}".format("Number of Gallons:", number_of_gallons))
    print("{0:28} ${1:,.2f}".format("Paint per Gallon:", paint_per_gallon))
    print("{0:28} ${1:,.2f}".format("Labor per Sq Ft:", labor_per_sqft ))
    print()

    #cost
    print("{0:17} {1:>8} {2:>18}".format("Cost" , "Amount", "Percentage"))
    print("{0:19} ${1:>,.2f} {2:>12.2%}".format("Paint:", paint_cost, paint_percent))
    print("{0:19} ${1:,.2f} {2:>11.2%}".format("Labor:", labor_cost, labor_percent))
    print("{0:19} ${1:,.2f} {2:>12.2%}".format("Total:", total_cost, total_percent))
    print() 
    
    # loop 
    again = (input("Estimat another paint job? (y/n): "))
    
    if (again == "y"):
        estimate_painting_cost()

    elif (again == "n"):
        print("Thank you for using our Painting Estimator!")
        print("Please see our website: https://bitbucket.org/agr18b/lis4369/src/master/")