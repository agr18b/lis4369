> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Assignment 5 Requirements:

*Three Parts:*

1. Backwards-engineer (using r) demo1.r demo2.r files given
2. The program shows basics of R, including:
    - Variables
    - Importing data
    - Creating tables from data

#### README.md file should include the following items:

* Screenshots of two graphs created using R.

#### Assignment Screenshots:

*Screenshot of Graph 1:

![Graph 1](img/1.PNG)

*Screenshot of Graph 2:

![Graph 2](img/2.PNG)