> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Project 2 Requirements:

*Two Parts:*

1. Code and run demo.r. (Note: *be sure( necessary packages are installed))
2. Use it to backward-engineer the screenshots provided in the assignment.

#### README.md file should include the following items:

* Screenshots of the Project 2 code working in RStudio

#### Assignment Screenshots:

*Screenshot of Project 2 application results:

![Part 1 screenshot of Project 2](img1.PNG)

![Part 2 screenshot of Project 2](img2.PNG)
