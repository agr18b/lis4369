> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course Title
LIS4369 - Extensible Enterprise Solutions

## Your Name
Andrew Reyes

*Course Work Links*

1. [A1 README.md](https://bitbucket.org/agr18b/lis4369/src/master/a1/)

    * Install Python
    * Install R
    * Install Visual Studio Code
    * Create a1_tip_calculator application
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations)
    * Provide git command description

2. [A2 README.md](https://bitbucket.org/agr18b/lis4369/src/master/a2/)

    * Payroll Calculator

3. [A3 README.md](https://bitbucket.org/agr18b/lis4369/src/master/a3/)

    * Painting Cost Calculator
    * Use print formatting for the code
    * Have the program repeat based on user input

4. [A4 README.md](https://bitbucket.org/agr18b/lis4369/src/master/a4/)

    * Use pandas for python
    * Use matplotlib for python
    * Pull data from defined website given in assignment guidelines
    * Print a graph from the data pulled

5. [A5 README.md](https://bitbucket.org/agr18b/lis4369/src/master/a5/)

    * Confirm R Install
    * Learn basics of R
    * Import and reate plots of provided Titanic dataset

6. [P1 README.md](https://bitbucket.org/agr18b/lis4369/src/master/p1/)

    * Download and install pandas for python
    * Download and install matplotlib for python
    * Use datetime library for python
    * Create chart from data pulled

7. [P2 README.md](https://bitbucket.org/agr18b/lis4369/src/master/p2/)

    * Use R Studio to create graphs
    * Use data from online source to populate graphs
    * Display graphs on Bitbucket