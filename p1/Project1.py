import pandas as pd
import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style

def program_requirements():
    print()
    print("Project 1")
    print()
    print("Program Requirements:")
    print("1. Run demo.py.")
    print("2. If errors, more than likely missing installations.")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Research how to do the following installations:")
    print("\ta. pandas (only if missing)")
    print("\tb. pandas-datareader (only if missing)")
    print("\tc. matplotlib (only if missing)")
    print("5. Create at least three functions that are called by the program:")
    print("\ta. main(): calls at least two other functions.")
    print("\tb. get_requirements(): displays the program requirements.")
    print("\tc. data_analysis_1(): displays the following data")

def data_analysis_1():
    start = datetime.datetime(2010,1,1)
    end = datetime.datetime.today()

    df = pdr.DataReader("XOM","yahoo", start,end)

    #get user data
    print("\nPrint number of records:")
    print(len(df.index))
    
    print("\nPrint columns:")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df) #note: for efficiency, only prints 60--not *all* records

    print("\nPrint first five lines:")
    print(df.head(5))

    print("\nPrint last five lines:")
    print(df.tail(5))

    print("\nPrint first two lines:")
    print(df.head(2))

    print("\nPrint last two lines:")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()