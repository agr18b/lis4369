> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Project 1 Requirements:

*Two Parts:*

1. Code and run demo.py. (Note: *be sure( necessary packages are installed))
2. Use it to backward-engineer the screenshots provided in the assignment.

#### README.md file should include the following items:

* Screenshots of the Project 1 code working

#### Assignment Screenshots:

*Screenshot of Project 1 application running:

![Part 1 screenshot of Project 1](img/1.png)

![Part 2 screenshot of Project 1](img/img2.PNG)

![Part 3 screenshot of Project 1](img/chart.PNG)
