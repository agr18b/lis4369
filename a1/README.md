> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links:
	- This assignment
	- the completed tutorial (bitbucketstationlocations)


#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* git commands with short descriptions

> #### Git commands w/short descriptions:

1. git init: creates a new git repository
2. git status: displays the state of the working directory and the staging area
3. git add: adds all modified files to the current directory
4. git commit: puts your changes into your local repo
5. git push: used to upload to local repository content to a remote repository.
6. git pull: used to fetch and download content from a remote repository to match that content
7. git config: a convenience function that is used to set git configuration values on a global or local project level. 


#### Assignment Screenshots:

*Screenshot of a1_tip_calculator application running (IDLE):

![IDLE Running Program Screenshot](img/idleCode.PNG)

*Screenshot of a1_tip_calculator application running (Visual Studio Code):

![Visual Studio Code Running Program Screenshot](img/VisualStudioCode.PNG)

