def main():
    print ("Square Feet to Acres\n")

    print ("Program Requirements:")
    print ("1. Research: number of square feet to acre of land.")
    print ("2. Must use float data type for user input and caluclations.")
    print ("3. Format and round conversion to two decimal places.\n")

    print ("Input:")
    squarefeet = float(input("Enter square feet: "))
    #print (squarefeet)

    #43560
    acres = squarefeet / 43560
    #print (acres)

    print ("\nOutput:")
    print ("{0:,.2f}".format(squarefeet), "square feet = ", "{0:,.2f}".format(acres), "acres")


main()
