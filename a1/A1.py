def main():

    print("Tip Calculator")
    print("\nProgram Requirements:")
    print("1. Must use float data type for user input except, party number")
    print("2. Must round calculations to two decimal places")
    print("3. Must format currency with dollar sign, and two decimal places")

    print("\nUser Input:\n")
    meal_cost = float(input("Cost of meal: "))
    tax_percent = float(input("Tax percent: "))
    tip_percent = float(input("Tip percent: "))
    people_num = int(input("Party number: "))

    #print (meal_cost, tax_percent, tip_percent, people_num)

    tax_amount = round(meal_cost * (tax_percent / 100), 2)
    due_amount = round(tax_amount + meal_cost, 2)
    tip_amount = round(due_amount * (tip_percent /100), 2)
    total = round(due_amount + tip_amount + tax_amount, 2)
    split_amount = round(total / people_num, 2)

    #print (tax_amount, due_amount, tip_amount, total, split_amount)

    print("\nProgram Output:")
    print("Subtotal:\t", "${0:,.2f}".format(meal_cost))
    print("Tax:\t\t", "${0:,.2f}".format(tax_amount))
    print("Amount Due:\t", "${0:,.2f}".format(due_amount))
    print("Gratuity:\t", "${0:,.2f}".format(tip_amount))
    print("Total:\t\t", "${0:,.2f}".format(total))
    print("Split Amount:\t", "${0:,.2f}".format(split_amount))

main()


