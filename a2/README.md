> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Andrew Reyes

### Assignment 2 Requirements:

*Three Parts:*

1. Backward-engineer (using Python) the following screenshots:
2. The program should be organized with two modules:
	- functions.py module contains the following functions:
		a. get_requirements()
		b. calculate_payroll()
		c. print_pay
	- main.py module imports the functions.py module, and calls the functions

#### README.md file should include the following items:

* Screenshots of payroll_calculator application running with and without overtime.

#### Assignment Screenshots:

*Screenshot of payroll_calculator application with no overtime:

![payroll calculator no overtime](img/img1.PNG)

*Screenshot of payroll_calculator application with overtime:

![payroll calculator with overtime](img/img2.PNG)

