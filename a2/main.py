import functions as functions

def main():
    functions.get_requirements()
    functions.calculate_payroll()

if __name__ == "__main__":
    main()